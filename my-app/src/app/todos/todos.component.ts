import { Component, OnInit } from '@angular/core';
import { Todo, TodosService } from '../services/todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  constructor(public todosService: TodosService) { }

  ngOnInit(): void {
    this.todosService.getTodos().subscribe()
  }
  deleteTodo(id:number){
    this.todosService.deleteTodos().subscribe(
      ()=>{
        this.todosService.todos.forEach((item, i)=> {
          if(item.id === id){
            this.todosService.todos.splice(i, 1)
          }
        })
      }
    )
 
  }
/*   deleteTodo(id:number){
    this.todosService.deleteTodos(id).subscribe(
      ()=>{
        this.todosService.todos.filter((item)=> item.id !== id)
      }
    )
  } */
}
