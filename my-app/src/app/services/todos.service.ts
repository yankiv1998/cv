import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import{tap} from 'rxjs/operators'

export interface Todo {
  id: number,
  userId: number,
  title: string,
  completed: string,
}

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  public todos: Todo[] = [];

  constructor(private http: HttpClient) { }

  getTodos(): Observable<Todo[]>{
    return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?_limit=5')
    .pipe(tap(todos => this.todos = todos))
  }

  deleteTodos(): Observable<Todo[]>{
    return this.http.delete<Todo[]>('https://jsonplaceholder.typicode.com/todos/${id}')
  }
  addTodos(todo:Todo): Observable<Todo>{
    return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', todo)
    .pipe()
  }
/*   deleteTodos(id: number): Observable<Todo[]>{
    return this.http.delete<Todo[]>('https://jsonplaceholder.typicode.com/todos/${id}')
    .pipe(tap(todos => this.todos = todos.filter((item) => item.id !== id)))
  } */
}
