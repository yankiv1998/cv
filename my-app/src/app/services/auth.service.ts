import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface User{
  name: string,
  username: string,
  email: string,
  phone: string,
  address: {
      street: string,
      suite: string,
      city: string,
      zipcode: string
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  register(user: User): Observable<User>{
    return this.http.post<User>('https://jsonplaceholder.typicode.com/users', user)
  }
}
