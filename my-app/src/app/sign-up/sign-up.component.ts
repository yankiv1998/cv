import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit, OnDestroy {

  form!: FormGroup;
  aSub!: Subscription
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      username: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      phone: new FormControl(null, [Validators.required, Validators.minLength(10)]),
  /*     address: new FormControl(null, [Validators.required]),
      suite: new FormGroup(null),
      city: new FormGroup(null, [Validators.required]),
      zipcode: new FormGroup(null, [Validators.required])
       */
    })
  }

  ngOnDestroy(): void {
    if(this.aSub){
      this.aSub.unsubscribe()
    }
  }

onSubmit(){
  this.form.disable()
  this.aSub = this.auth.register(this.form.value).subscribe(
    ()=> {
      this.router.navigate(['/todos'])
    },
    error =>{
      console.warn(error)
      this.form.enable()
    }
    
  )
}
}
