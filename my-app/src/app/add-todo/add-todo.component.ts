import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';



@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  openDialog(){
    const dialogRef = this.dialog.open(DialogBoxComponent,{
      width:'250px',
      data: {title: 'Add ToDo'}
    });
    dialogRef.afterClosed().subscribe(
      result => console.log('Todo was saved', result)
    )
  }

  ngOnInit(): void {
  }

}
