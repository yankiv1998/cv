import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TodosService } from '../services/todos.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss']
})
export class DialogBoxComponent implements OnInit {
  form!: FormGroup;
  tSub!: Subscription;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,private todosService: TodosService, private router: Router ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl(null, [Validators.required]),

    })
  }
    addToDo(){
      this.form.disable()
      this.tSub = this.todosService.addTodos(this.form.value).subscribe(
        ()=>{
          this.todosService.todos.push(this.form.value)
            }
      )
    }
  }


