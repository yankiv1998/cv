import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  contacts = [
    {name: 'Steve Sendun', number: '380988753158', birthday: '28.04.1994', email: 'sendun@gmail.com', address: 'Skruonuka 7'},
    {name: 'Natasha Ivanova', number: '380981134286', birthday: '28.04.1994', email: 'ivanova@gmail.com', address: 'Skruonuka 7'},
    {name: 'Tetiana Yankv', number: '380964871893', birthday: '19.10.1998', email: 'yaniv1998@gmail.com,', address: 'Skruonuka 7'}
  ]

  constructor() { }
  addContact(contact: any){
    this.contacts.push(contact)
  }
}
