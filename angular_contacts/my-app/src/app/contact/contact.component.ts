import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  @Input()
  contactItem!: { name: string, number: string, birthday: string, email: string, address: string; };

  constructor() { }

  ngOnInit(): void {
  }

}
