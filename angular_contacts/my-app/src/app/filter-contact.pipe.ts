import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterContact'
})
export class FilterContactPipe implements PipeTransform {

  transform(contactsList: any, searchStr: string) {
    if(searchStr.length === 0){
      return contactsList;
    }
   return  contactsList.filter((contact: { name: string; }) => contact.name.toLowerCase().indexOf(searchStr.toLowerCase()) !== -1);
  }

}
