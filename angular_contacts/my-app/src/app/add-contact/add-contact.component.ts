import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {

  contactName ='';
  contactNumber = '';
  @Output() onAddContact = new EventEmitter<{ name: string, number: string}>();

  constructor() { }

  ngOnInit(): void {
  }
  addContact(){
    this.onAddContact.emit({
      name: this.contactName,
      number: this.contactNumber,
    });
    this.contactName = '';
    this.contactNumber = '';
  }

}
