import { Component, OnInit } from '@angular/core';
import { ContactsService } from 'src/services/contacts.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],

})
export class ContactsComponent implements OnInit {
  searchContact = '';
  contacts: any = [];
  constructor(private service: ContactsService) { }

  ngOnInit(): void {
    this.contacts = this.service.contacts;
  }

  udateContactList(contact: any){
    this.service.addContact(contact)
  }

}
